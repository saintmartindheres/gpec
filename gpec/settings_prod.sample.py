"""
Django test settings for gpec project.
"""

# Import specific settings
from .settings import *

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'CHANGE WITH A STRONG KEY'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['gpec.saintmartindheres.fr']

# SECURITY WARNING
# https://docs.djangoproject.com/en/1.10/ref/settings/#csrf-cookie-httponly
CSRF_COOKIE_HTTPONLY = True

# SECURITY WARNING
# https://docs.djangoproject.com/en/1.10/ref/settings/#std:setting-SECURE_CONTENT_TYPE_NOSNIFF
SECURE_CONTENT_TYPE_NOSNIFF = True

# SECURITY WARNING
# https://docs.djangoproject.com/en/1.10/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True

# SECURITY WARNING
# https://docs.djangoproject.com/en/1.10/ref/clickjacking/#setting-x-frame-options-for-all-responses
X_FRAME_OPTIONS = 'DENY'


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'CHANGE WITH DATABASE USER',
        'USER': 'CHANGE WITH DATABASE NAME',
        'PASSWORD': 'CHANGE WITH DATABASE PASSWORD',
        'HOST': '',     # Do not change
        'PORT': '',     # Do not change
    }
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/home/gpec/gpec.saintmartindheres.fr/static/'

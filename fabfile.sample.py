# -*- coding: utf-8 -*-

'''
Ce fichier défini et execute la procédure à suivre pour déployer gpec
sur les serveurs de test et de production

Il faut avoir péalablement installé le logiciel fabric sur la machine à partir
de laquelle va être effectué le déploiement :
# sudo apt-get install python-fabric

Liste des action disponibles : 
# fab --list
'''

import datetime

from fabric.api import env, task, local, cd, run, put, sudo
from fabric.colors import green, yellow
from fabric.context_managers import prefix, warn_only

env.colorize_errors = True

# Apache
APACHE_SITES = '/etc/apache2/sites-available/'

# Where lives the entire project
PROJECT_PATH = ''.join(['/home/gpec/gpec.saintmartindheres.fr/'])
RELEASE_PATH = ''.join([PROJECT_PATH, 'src/'])

# Where lives python sources and binaries
VENV_PATH = ''.join([PROJECT_PATH, 'venv/'])
VENV_ACTIVATE_PATH = ''.join([VENV_PATH, 'bin/activate'])
VENV_PROXY_OPTION = '--proxy 10.33.0.9:8080'

# Django
REQUIREMENTS_PATH = ''.join([RELEASE_PATH, 'requirements.txt'])
GPEC_MODULE = ''.join([RELEASE_PATH, 'gpec/'])

env.release_path = RELEASE_PATH
env.host_string = "UNIX-USER@HOST"  # CHANGE THIS


def install_required_packages():
    sudo('apt install apache2 python3-venv python3-dev libapache2-mod-wsgi-py3 gettext postgresql postgresql-contrib libpq-dev gcc')


def create_directories():
    run('mkdir -p %s' % env.release_path)
    #sudo('chown -R www-data:www-data %s' % PROJECT_PATH)
    #sudo('chmod -R 775  %s' % PROJECT_PATH)
    print(green('[%s] Directories created' % env.host_string))


def upload_gpec_sources():
    local('git archive --format=tar master | gzip > gpec_release.tar.gz')
    put('gpec_release.tar.gz', PROJECT_PATH)
    run('rm -rf %s*' % env.release_path)
    with cd(env.release_path):
        run('tar -zxf ../gpec_release.tar.gz')
        run('rm ../gpec_release.tar.gz')
    local('rm gpec_release.tar.gz')
    print(green('[%s] Project files uploaded' % env.host_string))


def upload_django_settings():
    put('gpec/settings.py', ''.join([GPEC_MODULE, 'settings.py']))
    put('gpec/settings_prod.py', ''.join([GPEC_MODULE, 'settings_prod.py']))
    print(green('[%s] Django settings uploaded' % env.host_string))


def upload_wsgi_file():
    put('gpec/wsgi.prod.py', ''.join([GPEC_MODULE, 'wsgi.py']))
    print(green('[%s] Wsgi file uploaded' % env.host_string))


def upload_apache_conf():
    put('gpec/apache.prod.conf', ''.join([APACHE_SITES, 'gpec.saintmartindheres.fr.conf']), use_sudo=True)
    print(green('[%s] Apache configuration file uploaded' % env.host_string))


def create_python_virtualenv():
    try:
        run('python3 -m venv %s' % VENV_PATH)
        print(green('[%s] Virtual environment created' % env.host_string))
    except:
        run('python3 -m venv %s --upgrade' % VENV_PATH)
        print(yellow('[%s] Virtual environment allready created' % env.host_string))
        print(green('[%s] Virtual environment upgraded' % env.host_string))


def install_python_requirements():
    with prefix('source %s' % VENV_ACTIVATE_PATH):
        run('pip install %s --upgrade -qr %s' % (VENV_PROXY_OPTION, REQUIREMENTS_PATH))
    print(green('[%s] Python requirements installed' % env.host_string))


def collect_staticfiles():
    with cd(env.release_path), prefix('source %s' % VENV_ACTIVATE_PATH):
        run('python manage.py collectstatic -c --noinput -v0 --settings=gpec.settings_prod')
    print(green('[%s] Static files collected' % env.host_string))


def compile_messages():
    with cd(env.release_path), prefix('source %s' % VENV_ACTIVATE_PATH):
        run('python manage.py compilemessages --settings=gpec.settings_prod')
    print(green('[%s] Translation messages compiled' % env.host_string))


def check_security():
    with cd(env.release_path), prefix('source %s' % VENV_ACTIVATE_PATH):
        run('python manage.py check --deploy --settings=gpec.settings_prod')
    print(green('[%s] Security checked' % env.host_string))


def migrate_database():
    with cd(env.release_path), prefix('source %s' % VENV_ACTIVATE_PATH):
        run('python manage.py migrate --settings=gpec.settings_prod')
    print(green('[%s] Database migrated' % env.host_string))


def reload_apache():
    sudo('systemctl reload apache2.service')


@task
def disable_site():
    '''
    Désactive le site gpec.saintmartindheres.fr
    '''
    with warn_only():
        sudo('a2dissite gpec.saintmartindheres.fr.conf')
    reload_apache()
    print(yellow('[%s] Site disabled on Apache Server' % env.host_string))


@task
def enable_site():
    '''
    Active le site gpec.saintmartindheres.fr
    '''
    sudo('a2ensite gpec.saintmartindheres.fr.conf')
    reload_apache()
    print(green('[%s] Site enabled on Apache Server' % env.host_string))


@task
def deploy():
    '''
    Déploie la dernière version du site gpec.saintmartindheres.fr
    '''
    install_required_packages()
    create_directories()
    disable_site()

    upload_gpec_sources()
    upload_django_settings()
    upload_wsgi_file()

    create_python_virtualenv()
    install_python_requirements()
    migrate_database()
    collect_staticfiles()
    compile_messages()
    upload_apache_conf()

    enable_site()
    check_security()


@task
def create_superuser():
    '''
    Créé un super utilisateur pour le site gpec.saintmartindheres.fr
    '''
    with cd(env.release_path), prefix('source %s' % VENV_ACTIVATE_PATH):
        run('python manage.py createsuperuser --settings=gpec.settings_prod')
    print(green('[%s] SuperUser created' % env.host_string))

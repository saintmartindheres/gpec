from django.contrib.auth.hashers import make_password


def clean_user_data(model_fields):
    """
    Transforms the user data loaded from
    LDAP into a form suitable for creating a user.
    """
    # Create an unusable password for the user.
    model_fields["first_name"] = model_fields["first_name"].capitalize()
    model_fields["last_name"] = model_fields["last_name"].capitalize()
    model_fields["password"] = make_password(None)
    return model_fields

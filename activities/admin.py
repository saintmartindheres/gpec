from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _

from .models import Activity
from .models import Domain
from .models import Expertise
from .models import Quality
from .models import Skill


class GPECAdminSite(admin.AdminSite):
    site_header = _('GPEC')
    site_title = _('GPEC')
    site_url = None


class ExpertiseInline(admin.TabularInline):
    model = Activity.related_expertises.through
    extra = 0
    verbose_name = _('related expertise')
    verbose_name_plural = _('related expertises')


class ExpertiseAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ['__str__', 'name', 'domain_name']

    def domain_name(self, obj):
        return obj.domain.name

    domain_name.admin_order_field = 'domain__name'


class DomainAdmin(admin.ModelAdmin):
    search_fields = ['name']


class SkillInline(admin.TabularInline):
    model = Activity.related_skills.through
    extra = 0
    verbose_name = _('related skill')
    verbose_name_plural = _('related skills')


class SkillAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ['__str__', 'name', 'domain_name']

    def domain_name(self, obj):
        return obj.domain.name

    domain_name.admin_order_field = 'domain__name'


class ActivityAdmin(admin.ModelAdmin):
    actions = ['export_odf']
    exclude = ['related_expertises', 'related_skills']
    inlines = [ExpertiseInline, SkillInline]
    search_fields = ['name']
    list_display = ['__str__', 'name', 'domain_name']

    def domain_name(self, obj):
        return obj.domain.name

    domain_name.admin_order_field = 'domain__name'

    def export_odf(self, request, queryset):
        response = HttpResponse(queryset.export_as_ods(),
                                content_type="application/vnd.oasis.opendocument.spreadsheet")
        response['Content-Disposition'] = 'attachment; filename="gpec.ods"'

        return response
    export_odf.short_description = _('Export to odf')


class QualityAdmin(admin.ModelAdmin):
    search_fields = ['name']


class UserAdmin(admin.ModelAdmin):
    search_fields = ['email', 'username']
    list_display = ('username', 'first_name', 'last_name', 'email',
                    'is_staff', 'is_superuser')


admin.site = GPECAdminSite()
admin.site.register(Activity, ActivityAdmin)
admin.site.register(Domain, DomainAdmin)
admin.site.register(Expertise, ExpertiseAdmin)
admin.site.register(Quality, QualityAdmin)
admin.site.register(Skill, SkillAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Group)

from itertools import zip_longest

from odf.opendocument import OpenDocumentSpreadsheet
from odf.text import P
from odf.table import Table, TableRow, TableCell

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Domain(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'), unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('domain')
        verbose_name_plural = _('domains')


class Quality(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'), unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('quality')
        verbose_name_plural = _('qualities')


class Skill(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'), unique=True)
    domain = models.ForeignKey(Domain, verbose_name=_('domain'))

    def __str__(self):
        return " - ".join([self.domain.name, self.name])

    class Meta:
        verbose_name = _('skill')
        verbose_name_plural = _('skills')


class Expertise(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'), unique=True)
    domain = models.ForeignKey(Domain, verbose_name=_('domain'))

    def __str__(self):
        return " - ".join([self.domain.name, self.name])

    class Meta:
        verbose_name = _('expertise')
        verbose_name_plural = _('expertises')


class ActivityQuerySet(models.QuerySet):
    def export_as_ods(self):
        document = OpenDocumentSpreadsheet()

        table = Table(name=_("activities").title())
        header = (
            ("", _("activities"), _("competencies"), ""),
            (_("primary/specific"), _("title"), _("skills"), _("expertises")),
            ("", "", "", ""),
        )
        for line in header:
            row = TableRow()
            table.addElement(row)
            for element in line:
                cell = TableCell()
                row.addElement(cell)
                p = P(text=element.upper())
                cell.addElement(p)

        for activity in self:
            table = activity.export(table)
        document.spreadsheet.addElement(table)

        return document.xml()


class Activity(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'), unique=True)
    primary = models.BooleanField(verbose_name=_('primary activity'), default=True)
    domain = models.ForeignKey(Domain, verbose_name=_('domain'))
    related_skills = models.ManyToManyField(
        Skill,
        verbose_name=_('related skills'),
        blank=True
    )
    related_expertises = models.ManyToManyField(
        Expertise,
        verbose_name=_('related expertises'),
        blank=True
    )
    objects = ActivityQuerySet.as_manager()

    def __str__(self):
        return " - ".join([self.domain.name, self.name])

    def export(self, table):
        first_row = True
        if self.primary:
            primary = _("primary activity")
        else:
            primary = _("specific activity")
        for skill, expertise in zip_longest(self.related_skills.all(),
                                            self.related_expertises.all()):
            row = TableRow()
            table.addElement(row)
            if first_row:
                elements = [primary.title(), self, skill, expertise]
            else:
                elements = ["", "", skill, expertise]
            for element in elements:
                cell = TableCell()
                row.addElement(cell)
                p = P(text=element)
                cell.addElement(p)
            first_row = False
        row = TableRow()
        table.addElement(row)
        return table

    class Meta:
        verbose_name = _('activity')
        verbose_name_plural = _('activities')
